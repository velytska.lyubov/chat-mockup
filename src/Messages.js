import React from 'react';
import './Messages.css';

import Message from './Message';
import MessageDateSeparator from './MessageDateSeparator';
import ChannelHeadMessage from './ChannelHeadMessage';

import moment from 'moment';

const groupMessagesByDay = messages => messages.reduce((result, item) => {
  const day = moment(item.metadata.timestamp).startOf('day').format('YYYY-MM-DD');

  result[day] = [...(result[day] || []), item];

  return result;
}, {});

const messageToReactComponent = message => <Message key={message.id} message={message}/>;

const renderMessages = messages => {
  const groupByDay = groupMessagesByDay(messages);

  const content =
    Object.keys(groupByDay)
          .map(day => [
            <MessageDateSeparator key={`date-separator-${day}`} date={moment(day).toDate()}/>,
            ...groupByDay[day].map(messageToReactComponent)
          ])
          .reduce((result, item) => [...result, ...item], []);

  return content;
};

const Messages = ({ messages, channel }) => (
      <div id="messages">
        <div className="content">
          <ChannelHeadMessage channel={channel}/>
          {renderMessages(messages)}
        </div>
      </div>
    );




export default Messages;
