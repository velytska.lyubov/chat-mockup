import React, { Component } from 'react';
import './SidebarHomeMenu.css';

class SidebarHomeMenu extends Component {
  render () {
    return (
      <div className="home-container">
        <div className="home-menu">
          <div className="name-home">Home <span className="ion-ios-arrow-down"></span></div>
          <div className="user-name">
            <span className="ion-android-radio-button-on"></span> Jane
          </div>
        </div>
        <div className="bell"><span className="ion-android-notifications-none"></span></div>
      </div>

    );
  }
}

export default SidebarHomeMenu;
