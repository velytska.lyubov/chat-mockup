import React, { Component } from 'react';
import './Main.css';

import Header from './Header';
import Messages from './Messages';
import UserInput from './UserInput';

const messages = [
  {
    id: 1,
    body: 'test 1 2 3',
    user: {
      id: '1001',
      name: 'jane',
      avatar: null
    },
    metadata: {
      timestamp: new Date(2016, 1, 2, 17, 55)
    }
  },
  {
    id: 2,
    body: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora pariatur reiciendis maxime ab necessitatibus, nulla ipsa aut eveniet natus in vel accusantium nemo dolorum, impedit. Nobis ducimus neque libero sunt?`,
    user: {
      id: '1002',
      name: 'patrick',
      avatar: null
    },
    metadata: {
      timestamp: new Date(2017, 7, 20, 8, 17)
    }
  },
];

const channel = {
  id: 1,
  name: 'links-vacation',
  message: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur voluptate laboriosam expedita harum quasi, repellendus rerum quam tenetur, libero porro non explicabo ducimus, illum doloribus voluptatibus maiores et fuga aliquam.`,
  topic: `Lorem ipsum dolor sit amet -- TOPIC TOPIC!!!`,
  starred: true,
  stats: {
    totalUser: 500,
    activeUsers: 30,
    pinnedTopics: 85
  }
};

class Main extends Component {
  render () {
    return (
      <div id="main">
        <Header channel={channel}/>
        <Messages channel={channel} messages={messages} />
        <UserInput/>
      </div>
    );
  }
}

export default Main;
