import React from 'react';

const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

const MessageDateSeparator = ({ date }) => (
  <p className="line"><span>{date.toLocaleDateString('en-GB', options)}</span></p>
);

export default MessageDateSeparator;
