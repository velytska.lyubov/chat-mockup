import React, { Component } from 'react';
import './Sidebar.css';

import SidebarHomeMenu from './SidebarHomeMenu';
import SidebarAllThreads from './SidebarAllThreads';
import SidebarChannels from './SidebarChannels';
import SidebarDirectMessages from './SidebarDirectMessages';
import SidebarInvitePeople from './SidebarInvitePeople';
import SidebarApps from './SidebarApps';
import SidebarSearch from './SidebarSearch';

class Sidebar extends Component {
  render () {
    return (
      <div id="sidebar">
        <div className="sidebar-up">
          <SidebarHomeMenu/>
          <SidebarAllThreads/>
          <SidebarChannels/>
          <SidebarDirectMessages/>
          <SidebarInvitePeople/>
          <SidebarApps/>
        </div>
        <div className="sidebar-down">
          <SidebarSearch/>
        </div>
      </div>
    );
  }
}

export default Sidebar;
