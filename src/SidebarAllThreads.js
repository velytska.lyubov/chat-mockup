import React, { Component } from 'react';
import './SidebarAllThreads.css';

class SidebarAllThreads extends Component {
  render () {
    return (
      <div className="container-all-threads">
        <span className="ion-chatbubble-working"></span> All Threads
      </div>

    );
  }
}

export default SidebarAllThreads;
