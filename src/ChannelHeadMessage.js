import React from 'react';

const ChannelHeadMessage = ({ channel: { id, name, message } }) => (
  <div className="channel-head-message">
    <h2 className="channel-title">#{name}</h2>
    <div className="channel-info">{message}</div>
    <div className="channel-commands">
      <div className="channel-command">
        <span className="ion-android-create"></span>Set purpose</div>
      <div className="channel-command">
        <span className="ion-plus"></span>Add an app</div>
      <div className="channel-command">
        <span className="ion-person"></span>Invite others to this channel
      </div>
    </div>
  </div>
);

export default ChannelHeadMessage;
