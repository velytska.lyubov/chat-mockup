import React, { Component } from 'react';

class HeaderToolbar extends Component {
  render () {
    return (
      <div className="header-toolbar">
        <div className="icon telephone"><span className="ion-ios-telephone-outline"></span></div>
        <div className="icon i-small"><span className="ion-ios-information-outline"></span></div>
        <div className="icon settings-small"><span className="ion-gear-b"></span></div>
        <div className="input-search-input">
          <span className="ion-ios-search"></span>
          <input type="text" className="new-message-content" placeholder="Search"/>
        </div>
        <div className="icon email-small"><span className="ion-ios-at"> </span></div>
        <div className="icon star"><span className="ion-ios-star-outline"> </span></div>
        <div className="icon other-small"><span className="ion-android-more-vertical"> </span></div>
      </div>
    );
  }
}

export default HeaderToolbar;
