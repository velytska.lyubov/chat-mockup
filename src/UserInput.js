import React, { Component } from 'react';
import './UserInput.css';

class UserInput extends Component {
  render () {
    return (
      <div id="userinput">
        <div className="send-message">+</div>
        <input type="text" className="new-message-content" placeholder="Message #links-vacation"/>
        <div className="emotion">
        </div>
      </div>

    )
  }
}

export default UserInput;
