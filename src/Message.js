import React from 'react';

const Message = ({ message: { id, body, metadata, user } }) => (
  <div className="message" data-message-id={id}>
    <div className="avatar">
      {
        user.avatar
          ? <span className="ion-person"></span>
          : <span className="ion-person"></span>
      }

    </div>
    <div className="content">
      <div className="metadata">
        <div className="username">{user.name}</div>
        <div className="timestamp">{metadata.timestamp.toTimeString().substr(0, 5)}</div>
      </div>
      <div className="body">{body}</div>
    </div>
  </div>
);

export default Message;
