import React from 'react';
import './Header.css';
import HeaderChannelInfo from './HeaderChannelInfo';
import HeaderToolbar from './HeaderToolbar';

const Header = ({ channel }) => (
  <div id="header">
    <HeaderChannelInfo channel={channel} />
    <HeaderToolbar />
  </div>
);

export default Header;
