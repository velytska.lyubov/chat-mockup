import React, { Component } from 'react';
import './SidebarDirectMessages.css';

class SidebarDirectMessages extends Component {
  render () {
    return (
      <div className="container-direct-messages">
        <div className="name-direct-messages">Direct Messages
          <div className="container-list-of-users">
            <ul style={{ listStyleType: 'circle' }}>
              <li><span className="ion-android-favorite"></span> slackbot</li>
              <li><span className="ion-android-radio-button-on"></span> Jane</li>
              <li><span className="ion-android-radio-button-off"></span> Patrick</li>
            </ul>
          </div>
        </div>
        <div className="ion-ios-plus-outline"></div>
      </div>

    );
  }
}

export default SidebarDirectMessages;
