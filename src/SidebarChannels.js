import React, { Component } from 'react';
import './SidebarChannels.css';

class SidebarChannels extends Component {
  render () {
    return (
      <div className="container-channels">
        <div className="name-channels">Channels
          <div className="container-channels-names">
            <ul style={{ listStyleTypee: 'none' }}>
              <li># general</li>
              <li># kids</li>
              <li># kids-after-school</li>
              <li># links-germany</li>
              <li># links-vacation</li>
              <li># random</li>
            </ul>
          </div>
        </div>
        <div className="ion-ios-plus-outline"></div>
      </div>

    );
  }
}

export default SidebarChannels;
