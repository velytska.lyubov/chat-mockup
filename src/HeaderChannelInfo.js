import React from 'react';

const HeaderChannelInfo = ({ channel: { name, starred, stats: { totalUsers, activeUsers, pinnedTopics }, topic } }) => (
  <div className="header-channel-info">
    <div className="channel-name">#{name}</div>
    <div className="channel-stats">
      <div className="icon star-small"><span className={starred ? "ion-ios-star" : "ion-ios-star-outline"}> </span></div>
      <div className="icon person-small"><span className="ion-person"> {activeUsers}</span></div>
      <div className="icon counter-small"><span className="ion-pin"> {pinnedTopics}</span></div>
      {topic ? <span className="add-a-topic">{topic}</span> : <div className="add-a-topic">Add a topic</div>}
    </div>
  </div>
);

export default HeaderChannelInfo;
